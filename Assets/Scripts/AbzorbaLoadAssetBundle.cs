﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AbzorbaLoadAssetBundle : MonoBehaviour
{
    public void ButtonClicked()
    {
        // Change CRC and path or platform here as needed
        AssetBundleInfo fromLocalFile = new AssetBundleInfo()
        {
            crc = 0x719b4fc9, // CHANGE this
            url = Path.Combine(Path.Combine(Application.dataPath, @"..\"), @"\AssetBundles\StandaloneWindows\assetbundlewithsinglescene"), // CHANGE this
            version = 1,
            minimumSupportedAppVersion = 1
        };

        LoadAssetBundleAsObservable(fromLocalFile).Subscribe((loadedAssetBundle) =>
                       {
                           SceneManager.LoadScene("SceneUsingAltas");
                       });
    }

    public IObservable<AssetBundle> LoadAssetBundleAsObservable(AssetBundleInfo assetBundleInfo, IProgress<float> progress = null)
    {
        ReplaySubject<AssetBundle> observer = new ReplaySubject<AssetBundle>();

        var assetBundleObservable = UniRx.WebRequest.ObservableWebRequest.LoadFromCacheOrDownload(assetBundleInfo.url, assetBundleInfo.version, assetBundleInfo.crc, progress);

        assetBundleObservable.Subscribe((loadedBundle) =>
        {
            observer.OnNext(loadedBundle);
        });

        return observer;
    }

    public class AssetBundleInfo
    {
        public uint crc;
        public int minimumSupportedAppVersion;
        public string url;
        public uint version;
    }
}
