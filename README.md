# AssetBundleWorkflow

This is a sample project to demonstrate the asset bundle workflow with atlases.

## Goal

1. Have a scene with images loaded from altas [SceneUsingAltas].
2. Be able to edit and work on that scene [SceneUsingAltas] in the editor.
3. Convert said screen [SceneUsingAltas] to asset bundle that is loaded from a server. Ensuring that images atlases are NOT included in the build.
4. Load and run scene from asset bundle on Android, iOS.

## The sample project

* Scene [SceneUsingAltas] that displays 3 images from an atlas. That scene is by itself an asset bundle.
* Scene [SceneThatLoadsAssetBundle] that loads locally (now configured for windows) an asset bundle and loads the scene [SceneUsingAltas].
* Altas [SampleAtlas] that contains 3 images (located in the Textures folder).

## The problem

Depending on what option **Include in Build** we select for the atlas, there is never a scenario where everything works as expected.

### Atlas option - Include in Build [x]

#### Editor

```diff
+ [SceneUsingAltas] is running fine.
- Asset bundle does NOT load.
```

#### Runtime

```diff
+ Asset bundle loads.
- Atlas is included in the build.
```

### Atlas option - Include in Build [ ]

#### Editor

```diff
- [SceneUsingAltas] does NOT run.
- Asset bundle does NOT load.
```

#### Runtime

```diff
+ Asset bundle loads.
+ Atlas is not included in the build.
```

## Notes

Feel free to change the demo files to build for Mac / iOS / Android

